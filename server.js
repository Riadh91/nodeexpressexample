const express = require("express");
const MongoConnect = require("./utils/db");
const { port } = require("./config/config");
const bodyParser = require("body-parser");

const server = express();

const document = require("./routes/document");

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

server.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "POST,GET,OPTIONS,DELETE,PATCH"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  if (req.method === "OPTIONS") {
    return res.sendStatus(200);
  }
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization, authorization"
  );
  next();
});

server.use((error, req, res, next) => {
  const status = error.statusCode || 500;
  const message = error.message;
  res.status(status).json({ message: message });
});

server.get("/healthcheck", (req, res) => {
  res.status(200).json({
    message: "the server is up"
  });
});

server.use("/api/document", document);


const main = async () => {
  try {
    const connection = await MongoConnect();
    if (connection) {
      console.log("Data Base connected !");
      server.listen(port, () => console.log(`Server Running on port ${port}`));
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};
main();
