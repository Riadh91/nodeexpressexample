const mongoose = require("mongoose");

const MongoConnect = async () => {
  try {
    return await mongoose.connect(`mongodb://localhost/DocumentApp`, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
  } catch (err) {
    console.log("MongoDb error is:", err);
    throw err;
  }
};

module.exports = MongoConnect;
