const s3 = require("../config/s3.config");

exports.upload = (files, unique) => {
  const data = files;
  const s3Client = s3.s3Client;
  const params = s3.uploadParams;

  let docUrls = [];
  return new Promise((resolve, reject) => {
    data.forEach(async doc => {
      
      const fileName = doc.originalname.split(".")[0];
      const fileExtention = doc.originalname.split(".")[1];
      try {
        params.Key = `document/documents${unique}/${unique}--${fileName}.${fileExtention}`;
        params.Body = doc.buffer;
        const upload = await s3Client.upload(params).promise();
        if (upload) {
          const info = {
            filename: doc.originalname,
            key: upload.key,
            location: upload.Location
          };
          docUrls.push(info);
        }
        if (docUrls.length === data.length) {
          resolve(docUrls);
        }
      } catch (err) {
        console.log(err);
        reject(err);
      }
    });
  });
};
