const mongoose = require("mongoose");
const Document = require("../models/document");
const { upload } = require("../utils/upload");
const uuidv1 = require("uuid/v1");

exports.createDocument = async (req, res) => {
  
  const { firstName, lastName } = JSON.parse(req.body.user);
  const files = req.files;

  let fileUrls = [];
  const unique = uuidv1()
    .split("-")
    .pop();

  try {
    if (files.length > 0) {
      fileUrls = await upload(files, unique);
    }

    const newDoc = new Document({
      firstName: firstName,
      lastName: lastName,
      files: fileUrls
    });

    const result = await newDoc.save();
    res.status(200).json(result);

  } catch (err) {
    console.log(err);
    res.status(400).json({ err });
  }
};
