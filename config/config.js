const dotenv = require('dotenv');
dotenv.config();
module.exports = {
    port: process.env.PORT,
    ENV: process.env.NODE_ENV,
    MongoDBUrl: process.env.MONGO_DB,
};