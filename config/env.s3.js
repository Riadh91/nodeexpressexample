const env = {
	AWS_ACCESS_KEY: 'Your Key',
	AWS_SECRET_ACCESS_KEY: 'Your Key',
	REGION : 'Your Region',
	Bucket: 'Your Bucket name'
};

module.exports = env;