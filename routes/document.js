const express = require('express');
const router = express.Router();

const upload = require('../config/multer.config.js');
const {createDocument} = require('../controllers/document.js');

router.post('/newdocument', upload.array("file"), createDocument);

module.exports = router;